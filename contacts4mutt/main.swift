//
//  main.swift
//  contacts4mutt
//
//  Created by Bradley McDuffie on 9/1/21.
//

import Foundation
import Contacts

struct Contact {
    var Name: String
    var Addresses: [String]
}

var contacts = [Contact]()

if CommandLine.arguments.count < 2 {
    let components = CommandLine.arguments[0].components(separatedBy: "/")
    print("Usage: \(components[components.count - 1]) searchterm")
    exit(0)
}

let searchString = CommandLine.arguments[1]

let keysToFetch = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactEmailAddressesKey] as [CNKeyDescriptor]

let store = CNContactStore()
do {
    let predicate = CNContact.predicateForContacts(matchingName: searchString)
    let foundContacts = try store.unifiedContacts(matching: predicate, keysToFetch: keysToFetch)
    for found in foundContacts {
        var contact = Contact(Name: "\(found.givenName) \(found.familyName)", Addresses: [])
        
        for email in found.emailAddresses {
            if isValidEmail(email.value as String) {
                if contact.Addresses.contains(email.value as String) == false {
                    contact.Addresses.append(email.value as String)
                }
            }
        }

        contact.Addresses.sort()
        contacts.append(contact)
    }
}

if contacts.count > 0 {
    contacts.sort { $0.Name < $1.Name }
    print("Found: \(contacts.count)")
    for contact in contacts {
        for email in contact.Addresses {
            print("\(email)\t\(contact.Name)")
        }
    }

}

func isValidEmail(_ email: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

    let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailPred.evaluate(with: email)
}
