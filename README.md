contacts4mutt
=============
`contacts4mutt` is an application I threw together to replace a utility I had working a long time ago (https://github.com/narkoleptik/abtomutt) to pull email addresses from the macOS Contacts app to use in [`mutt`](https://mutt.org). Since the libraries for that one are out of date, I decided to just re-implement it in swift, and wow, it was pretty easy to do.

# Usage
`contacts4mutt searchterm`
```
$ contacts4mutt apple
Found 2
donotreply@apple.com    Apple, Inc
johnnyappleseed@127.0.0.1   Johnny Appleseed
```
